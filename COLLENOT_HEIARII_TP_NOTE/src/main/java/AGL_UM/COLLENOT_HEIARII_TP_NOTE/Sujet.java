package AGL_UM.COLLENOT_HEIARII_TP_NOTE;

public class Sujet {
	  //ATTRIBUTS
	  private int id;
	  private String titre;
	  private Groupe groupeApartenance;

	  //CONSTRUCTEUR

	  Sujet (int i, String t){
	    this.id = i;
	    this.titre = t;
	    this.groupeApartenance = null;
	  }

	  //GETTERS & SETTERS

	  public int getId(){
	    return this.id;
	  }

	  public String getTitre(){
	    return this.titre;
	  }

	  public void setId(int id){
	    this.id = id;
	  }

	  public void setTitre(String titre){
	    this.titre = titre;
	  }
	  
	//================================ pour le tp noté ===================
	  
	  public void setGroupe(Groupe g) {
		  
		  this.groupeApartenance = g;
	  }
	  
	  public Groupe getGroupe() {
		  return this.groupeApartenance;
	  }
	  
	}