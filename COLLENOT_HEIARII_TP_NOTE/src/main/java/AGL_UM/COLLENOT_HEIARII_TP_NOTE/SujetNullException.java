package AGL_UM.COLLENOT_HEIARII_TP_NOTE;

@SuppressWarnings("serial")
public class SujetNullException extends Exception {

	public SujetNullException(String m) {
		
		super(m);
	}
	
}
