package AGL_UM.COLLENOT_HEIARII_TP_NOTE;


import java.util.ArrayList;
public class Groupe{

  //ATTRIBUTS

  private int id;
  private String nom;

  private ArrayList<Sujet> listeVoeux;
  private Sujet sujetFinal;

  //CONSTRUCTEUR

  public Groupe(){

    this.id = -1;
    this.nom = "rien";
    this.listeVoeux = new ArrayList<Sujet>();
  }

  public Groupe(int id, String nom){

    this.id = id;
    this.nom = nom;
    this.listeVoeux = new ArrayList<Sujet>();
    this.sujetFinal = null;
  }

  //GETTERS & SETTERS

  public int getId(){

    return this.id;
  }

  public String getNom(){

    return this.nom;
  }

  public ArrayList getListeVoeux(){

    return this.listeVoeux;
  }

  public void setId(int id){

    this.id = id;
  }

  public void setNom(String nom){

    this.nom = nom;
  }

  //METHODES

  public void afficherListe(){
    for(int i = 0; i<this.listeVoeux.size()-1; i++){
      System.out.println(this.listeVoeux.get(i));
      System.out.println(", ");
    }
    System.out.println(this.listeVoeux.get(this.listeVoeux.size()-1));  }

  public void ajouterVoeux(Sujet sujet){

    if(this.listeVoeux.size() > 4)
      
      System.out.println("ERREUR: Vous avez deja fait 5 voeux!");
    
    else{

      this.listeVoeux.add(sujet);
    }
  }

  public void soumettreVoeux(){
    
    System.out.println("Envoie de la liste...");
  }
  
  //================================ pour le tp noté ===================
  
  public Sujet getSujetFinal() throws SujetNullException {
	  
	  if(this.sujetFinal == null)
		  throw new SujetNullException("Ce groupe n'a pas de sujet");
	  else
	  return this.sujetFinal;
  }
  
  
  public void setSujetFinal(Sujet sujetFinal) {
	  
	  this.sujetFinal = sujetFinal;
	  sujetFinal.setGroupe(this);
  }
  

}