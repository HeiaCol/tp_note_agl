package AGL_UM.COLLENOT_HEIARII_TP_NOTE;

public class Voeux{

	//ATTRIBUTS

	private int ordre;
	private Sujet choix;

	//CONSTRUCTEUR
	  
	public Voeux(int o, Sujet s){
	this.ordre = o;
	this.choix = s;
	}

	//GETTERS & SETTERS

	public int getOrdre(){
	  return ordre;
	}

	public Sujet getSujet(){
	  return choix;
	}

	public void setOrdre(int ordre){
	  this.ordre = ordre;
	}

	public void setSujet(Sujet choix){
	  this.choix = choix;
	}


	}