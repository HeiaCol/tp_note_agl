package AGL_UM.COLLENOT_HEIARII_TP_NOTE;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class TestSujetGroupe {

	private Groupe groupe1;
	private Groupe groupe2;
	private Sujet sujet1;
	private Sujet sujet2;
	
	private boolean alreadyInit = false;
	
	@BeforeEach
	public void initAll() {
		
if(alreadyInit) return;
		
		else {
			groupe1 = new Groupe(1, "LE groupe des gens forts");
			groupe2 = new Groupe(2, "Team 1er degre");
		
			sujet1 = new Sujet(1, "Le developpement durable");
			sujet2 = new Sujet(2, "Pourquoi Amazon est-il une revolution ?");
			
			alreadyInit = true;
		}
		
	}
	
	@Test
	void testSujetNull() {
		
			assertThrows(SujetNullException.class, () -> {
			
			groupe1.getSujetFinal();
		});
	}
	
	@Test
	void testSujetGroupe() throws SujetNullException {
		
		groupe2.setSujetFinal(sujet1);	//on donne le sujet1 au groupe2
		
		assertEquals(groupe2.getSujetFinal(), sujet1);
		assertEquals(sujet1.getGroupe(), groupe2);
		
	}
	
	//question 5: pour augmenter la couverture de tests on pourrait tester des fonctions pour empecher 2 groupes d'avoir le meme sujets
	//ou encore si un groupe en a deja un et rajouter une classe GroupeDejaUnSujetException, ...

}
